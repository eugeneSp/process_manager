#ifndef TESTS_HPP_
#define TESTS_HPP_
#include <pthread.h>

void TestSemaphore() {
    HANDLE threads[10];
    semaphore = CreateSemaphore(NULL, 2, 2, NULL);

    for (int i = 0; i < 10; i++) {
        threads[i] = CreateThread(NULL, 0, threadFunctionSemaphore, reinterpret_cast<LPVOID>(static_cast<INT_PTR>(i)), 0, NULL);
    }

    WaitForMultipleObjects(10, threads, TRUE, INFINITE);

    for (int i = 0; i < 10; i++) {
        CloseHandle(threads[i]);
    }

    CloseHandle(semaphore); // Destroying the semaphore
}
void TestCriticalSection() {
    InitializeCriticalSection(&criticalSection);

    pthread_t thread1, thread2, thread3, thread4, thread5, thread6, thread7, thread8, thread9, thread10;

    pthread_create(&thread1, NULL, threadFunctionCriticalSection, (void*)1);
    pthread_create(&thread2, NULL, threadFunctionCriticalSection, (void*)2);
    pthread_create(&thread3, NULL, threadFunctionCriticalSection, (void*)3);
    pthread_create(&thread4, NULL, threadFunctionCriticalSection, (void*)4);
    pthread_create(&thread5, NULL, threadFunctionCriticalSection, (void*)5);
    pthread_create(&thread6, NULL, threadFunctionCriticalSection, (void*)6);
    pthread_create(&thread7, NULL, threadFunctionCriticalSection, (void*)7);
    pthread_create(&thread8, NULL, threadFunctionCriticalSection, (void*)8);
    pthread_create(&thread9, NULL, threadFunctionCriticalSection, (void*)9);
    pthread_create(&thread10, NULL, threadFunctionCriticalSection, (void*)10);

    pthread_join(thread1, NULL);
    pthread_join(thread2, NULL);
    pthread_join(thread3, NULL);
    pthread_join(thread4, NULL);
    pthread_join(thread5, NULL);
    pthread_join(thread6, NULL);
    pthread_join(thread7, NULL);
    pthread_join(thread8, NULL);
    pthread_join(thread9, NULL);
    pthread_join(thread10, NULL);

    DeleteCriticalSection(&criticalSection);
}

void TestMutex() {
    const int numThreads = 10;
    std::thread threads[numThreads];

    for (int i = 0; i < numThreads; i++) {
        threads[i] = std::thread(threadFunctionMutex);
    }

    for (int i = 0; i < numThreads; i++) {
        threads[i].join();
    }
}

#endif