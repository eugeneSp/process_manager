#ifndef PROCESS_HPP_
#define PROCESS_HPP_

#include <iostream>
#include <Windows.h>
#include <TlHelp32.h>
#include <semaphore.h>
#include <thread>
#include <cstdio>
#include <mutex>


void createProcess(const char* applicationName);
void printCurrentProcesses();
void terminateProcess(DWORD processId);
void modifyProcessPriority(DWORD processId, DWORD priority);
void suspendProcess(DWORD processId);
void resumeProcess(DWORD processId);

#endif