#ifndef THREADS_HPP_
#define THREADS_HPP_

#include "process.hpp"

void suspendThread(DWORD threadId);
void suspendThread(DWORD threadId);
void resumeThread(DWORD threadId);
void terminateThread(DWORD threadId);
void printThreads(DWORD processId);
void setThreadPriority(DWORD threadId, int priority);

#endif