#ifndef PROCESS_MANAGER_HPP_
#define PROCESS_MANAGER_HPP_

#include "threads.hpp"

HANDLE semaphore;
CRITICAL_SECTION criticalSection;
std::mutex mtx; // Declare a mutex object

DWORD WINAPI threadFunctionSemaphore(LPVOID arg) {
    WaitForSingleObject(semaphore, INFINITE);
    std::cout << "The " << reinterpret_cast<long long int>(arg) << " stream has entered the critical section\n";
    std::this_thread::sleep_for(std::chrono::seconds(2)); // Simulating work in the critical section
    ReleaseSemaphore(semaphore, 1, NULL);
    std::cout << "The " << reinterpret_cast<long long int>(arg) << " stream has left the critical section\n";
    return 0;
}

void* threadFunctionCriticalSection(void* arg) {
    EnterCriticalSection(&criticalSection);
    std::cout << "The " << reinterpret_cast<long long int>(arg) << " stream has entered the critical section\n";
    std::this_thread::sleep_for(std::chrono::seconds(2)); // Simulating work in the critical section
    LeaveCriticalSection(&criticalSection);
    std::cout << "The " << reinterpret_cast<long long int>(arg) << " stream has left the critical section\n";
    return NULL;
}

void threadFunctionMutex() {
    std::lock_guard<std::mutex> lock(mtx); // Lock the mutex

    // Critical section
    std::cout << "Thread " << std::this_thread::get_id() << " entered the critical section" << std::endl;
    std::this_thread::sleep_for(std::chrono::seconds(2)); // Simulating work in the critical section
    std::cout << "Thread " << std::this_thread::get_id() << " left the critical section" << std::endl;

    // The mutex will be automatically unlocked when the lock_guard goes out of scope
}

#endif