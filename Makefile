FLAGS = -std=c++17 -Wall -Werror -Wextra

start:
	g++ $(FLAGS) -o app/app src/*.cpp && cd app && app.exe && cd ..

clean:
	cd app && del app.exe && cd ..

clang:
	clang-format -i --style=Google *.cpp