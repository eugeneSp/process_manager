#include "./../libraries/process.hpp"

void createProcess(const char* applicationName) {
    STARTUPINFOA startupInfo;
    PROCESS_INFORMATION processInfo;

    ZeroMemory(&startupInfo, sizeof(startupInfo));
    startupInfo.cb = sizeof(startupInfo);
    ZeroMemory(&processInfo, sizeof(processInfo));

    // Create the process
    if (!CreateProcessA(
        NULL,                   // No module name (use command line)
        const_cast<char*>(applicationName),  // Command line
        NULL,                   // Process handle not inheritable
        NULL,                   // Thread handle not inheritable
        FALSE,                  // Set handle inheritance to FALSE
        0,                      // No creation flags
        NULL,                   // Use parent's environment block
        NULL,                   // Use parent's starting directory
        &startupInfo,           // Pointer to STARTUPINFO structure
        &processInfo            // Pointer to PROCESS_INFORMATION structure
    )) {
        std::cout << "Failed to create process. Error code: " << GetLastError() << std::endl;
        return;
    }

    // Close process and thread handles
    CloseHandle(processInfo.hProcess);
    CloseHandle(processInfo.hThread);
}

void printCurrentProcesses() {
    HANDLE hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
    if (hSnapshot != INVALID_HANDLE_VALUE) {
        PROCESSENTRY32 processEntry;
        processEntry.dwSize = sizeof(processEntry);

        if (Process32First(hSnapshot, &processEntry)) {
            std::cout << "Current Process List:\n";
            std::cout << "--------------------\n";
            do {
                std::cout << processEntry.th32ProcessID << ": " << processEntry.szExeFile << "\n";
            } while (Process32Next(hSnapshot, &processEntry));
        }

        CloseHandle(hSnapshot);
    }
}

void terminateProcess(DWORD processId) {
    HANDLE hProcess = OpenProcess(PROCESS_TERMINATE, FALSE, processId);
    if (hProcess != NULL) {
        TerminateProcess(hProcess, 0);
        CloseHandle(hProcess);
    }
}

void modifyProcessPriority(DWORD processId, DWORD priority) {
    HANDLE hProcess = OpenProcess(PROCESS_SET_INFORMATION, FALSE, processId);
    if (hProcess != NULL) {
        if (priority == 1) {
            SetPriorityClass(hProcess, HIGH_PRIORITY_CLASS);
        } else if (priority == 2) {
            SetPriorityClass(hProcess, NORMAL_PRIORITY_CLASS);
        } else if (priority == 3) {
            SetPriorityClass(hProcess, IDLE_PRIORITY_CLASS);
        } else if (priority == 4) {
            SetPriorityClass(hProcess, REALTIME_PRIORITY_CLASS);
        } else if (priority == 5) {
            SetPriorityClass(hProcess, BELOW_NORMAL_PRIORITY_CLASS);
        } else if (priority == 6) {
            SetPriorityClass(hProcess, ABOVE_NORMAL_PRIORITY_CLASS);
        } else {
            std::cout << "Invalid priority value" << std::endl;
        }
        CloseHandle(hProcess);
    }
}

void suspendProcess(DWORD processId) {
    HANDLE hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPTHREAD, 0);
    if (hSnapshot != INVALID_HANDLE_VALUE) {
        THREADENTRY32 threadEntry;
        threadEntry.dwSize = sizeof(threadEntry);

        if (Thread32First(hSnapshot, &threadEntry)) {
            do {
                if (threadEntry.th32OwnerProcessID == processId) {
                    HANDLE hThread = OpenThread(THREAD_SUSPEND_RESUME, FALSE, threadEntry.th32ThreadID);
                    if (hThread != NULL) {
                        SuspendThread(hThread);
                        CloseHandle(hThread);
                    }
                }
            } while (Thread32Next(hSnapshot, &threadEntry));
        }

        CloseHandle(hSnapshot);
    }
}

void resumeProcess(DWORD processId) {
    HANDLE hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPTHREAD, 0);
    if (hSnapshot != INVALID_HANDLE_VALUE) {
        THREADENTRY32 threadEntry;
        threadEntry.dwSize = sizeof(threadEntry);

        if (Thread32First(hSnapshot, &threadEntry)) {
            do {
                if (threadEntry.th32OwnerProcessID == processId) {
                    HANDLE hThread = OpenThread(THREAD_SUSPEND_RESUME, FALSE, threadEntry.th32ThreadID);
                    if (hThread != NULL) {
                        ResumeThread(hThread);
                        CloseHandle(hThread);
                    }
                }
            } while (Thread32Next(hSnapshot, &threadEntry));
        }

        CloseHandle(hSnapshot);
    }
}