#include <iostream>
#include <windows.h>
#include <tlhelp32.h>
#include "./../libraries/process_manager.hpp"
#include "./../test/tests.hpp"

int main() {
    int lastPrintedProccesId = 0;
    int choice;
    std::string input;
    DWORD processId;

    printCurrentProcesses();

    while (true) {
        system("cls");
        std::cout << "0. Create Process\n"
                     "1. Terminate Process\n"
                     "2. Modify Process Priority\n"
                     "3. Suspend Process\n"
                     "4. Resume Process\n"
                     "5. Terminate Thread\n"
                     "6. Suspend Thread\n"
                     "7. Resume Thread\n"
                     "8. Print Threads\n"
                     "9. Semaphore Test\n"
                     "10. Mutex Test\n"
                     "11. Critical Section Test\n"
                     "12. Print all processes\n"
                     "13. Change priority of thread\n"
                     "14. Exit\n"
                     "Last written process: " << lastPrintedProccesId << "\n";
        std::cout << "Enter your choice: ";
        std::getline(std::cin, input);

        try {
            choice = std::stoi(input);
        } catch (std::invalid_argument& e) {
            std::cout << "Invalid input. Please enter a number.\n";
            continue;
        }
        switch (choice) {
        case 0:
            createProcess("C:\\Windows\\System32\\notepad.exe");
            break;
        case 1:
            std::cout << "Enter the process ID: ";
            std::cin >> processId;
            lastPrintedProccesId = processId;
            terminateProcess(processId);
            break;
        case 2:
            std::cout << "Enter the process ID: ";
            std::cin >> processId;
            lastPrintedProccesId = processId;
            std::cout << "Enter the priority:\n"
                      "1 - HIGH_PRIORITY_CLASS\n"
                      "2 - NORMAL_PRIORITY_CLASS\n"
                      "3 - IDLE_PRIORITY_CLASS\n"
                      "4 - REALTIME_PRIORITY_CLASS\n"
                      "5 - BELOW_NORMAL_PRIORITY_CLASS\n"
                      "6 - ABOVE_NORMAL_PRIORITY_CLASS \n"
                      "I wanna: ";
            int priority;
            std::cin >> priority;
            modifyProcessPriority(processId, priority);
            break;
        case 3:
            std::cout << "Enter the process ID: ";
            std::cin >> processId;
            lastPrintedProccesId = processId;
            suspendProcess(processId);
            break;
        case 4:
            std::cout << "Enter the process ID: ";
            std::cin >> processId;
            lastPrintedProccesId = processId;
            resumeProcess(processId);
            break;
        case 5:
            std::cout << "Enter the process ID: ";
            std::cin >> processId;
            lastPrintedProccesId = processId;
            printThreads(processId);
            std::cout << "Enter the thread ID: ";
            DWORD threadId;
            std::cin >> threadId;
            terminateThread(threadId);
            break;
        case 6:
            std::cout << "Enter the process ID: ";
            std::cin >> processId;
            lastPrintedProccesId = processId;
            printThreads(processId);
            std::cout << "Enter the thread ID: ";
            std::cin >> threadId;
            suspendThread(threadId);
            break;
        case 7:
            std::cout << "Enter the process ID: ";
            std::cin >> processId;
            lastPrintedProccesId = processId;
            printThreads(processId);
            std::cout << "Enter the thread ID: ";
            std::cin >> threadId;
            resumeThread(threadId);
            break;
        case 8:
            std::cout << "Enter the process ID: ";
            std::cin >> processId;
            lastPrintedProccesId = processId;
            printThreads(processId);
            while(std::getchar() != 'q');
            break;
        case 9:
            TestSemaphore();
            break;
        case 10:
            TestMutex();
            break;
        case 11:
            TestCriticalSection();
            break;
        case 12:
            printCurrentProcesses();
            while(std::getchar() != 'q');
            break;
        case 13:
            std::cout << "Enter the process ID: ";
            std::cin >> processId;
            printThreads(processId);
            std::cout << "Enter the thread ID: ";
            std::cin >> threadId;
            std::cout << "Enter the priority:\n"
                    "1 - THREAD_PRIORITY_TIME_CRITICAL\n"
                    "2 - THREAD_PRIORITY_HIGHEST\n"
                    "3 - THREAD_PRIORITY_ABOVE_NORMAL\n"
                    "4 - THREAD_PRIORITY_NORMAL\n"
                    "5 - THREAD_PRIORITY_BELOW_NORMAL\n"
                    "6 - THREAD_PRIORITY_LOWEST\n"
                    "7 - THREAD_PRIORITY_IDLE \n"
                    "I wanna: ";
            int threadPriority;
            std::cin >> threadPriority;
            if (threadPriority == 1) {
                setThreadPriority(threadId, THREAD_PRIORITY_TIME_CRITICAL);
            } else if (threadPriority == 2) {
                setThreadPriority(threadId, THREAD_PRIORITY_HIGHEST);
            } else if (threadPriority == 3) {
                setThreadPriority(threadId, THREAD_PRIORITY_ABOVE_NORMAL);
            } else if (threadPriority == 4) {
                setThreadPriority(threadId, THREAD_PRIORITY_NORMAL);
            } else if (threadPriority == 5) {
                setThreadPriority(threadId, THREAD_PRIORITY_BELOW_NORMAL);
            } else if (threadPriority == 6) {
                setThreadPriority(threadId, THREAD_PRIORITY_LOWEST);
            } else if (threadPriority == 7) {
                setThreadPriority(threadId, THREAD_PRIORITY_IDLE);
            } else {
                std::cerr << "Invalid priority value.\n";
            }
            break;
        case 14:
            exit(0);
        default:
            std::cout << "Invalid choice. Please try again\n";
        }
    }

    return 0;
}