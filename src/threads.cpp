#include "./../libraries/threads.hpp"

void suspendThread(DWORD threadId) {
    HANDLE hThread = OpenThread(THREAD_SUSPEND_RESUME, FALSE, threadId);
    if (hThread != NULL) {
        SuspendThread(hThread);
        CloseHandle(hThread);
    }
}

// Function to resume a thread by its ID
void resumeThread(DWORD threadId) {
    HANDLE hThread = OpenThread(THREAD_SUSPEND_RESUME, FALSE, threadId);
    if (hThread != NULL) {
        ResumeThread(hThread);
        CloseHandle(hThread);
    }
}

// Function to terminate a thread by its ID
void terminateThread(DWORD threadId) {
    HANDLE hThread = OpenThread(THREAD_TERMINATE, FALSE, threadId);
    if (hThread != NULL) {
        TerminateThread(hThread, 0);
        CloseHandle(hThread);
    }
}

// Function to print the IDs and states of all threads in a process
void printThreads(DWORD processId) {
    HANDLE hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPTHREAD, 0);
    if (hSnapshot != INVALID_HANDLE_VALUE) {
        THREADENTRY32 threadEntry;
        threadEntry.dwSize = sizeof(threadEntry);

        if (Thread32First(hSnapshot, &threadEntry)) {
            std::cout << "Threads in process " << processId << ":\n";
            std::cout << "--------------------------------------------\n";
            do {
                if (threadEntry.th32OwnerProcessID == processId) {
                    HANDLE hThread = OpenThread(THREAD_QUERY_INFORMATION, FALSE, threadEntry.th32ThreadID);
                    if (hThread != NULL) {
                        int priority = GetThreadPriority(hThread);
                        std::cout << threadEntry.th32ThreadID << ": " << priority << "\n";
                        CloseHandle(hThread);
                    }
                }
            } while (Thread32Next(hSnapshot, &threadEntry));
        }

        CloseHandle(hSnapshot);
    }
}

void setThreadPriority(DWORD threadId, int priority) {
    HANDLE hThread = OpenThread(THREAD_SET_INFORMATION, FALSE, threadId);
    if (hThread != NULL) {
        if (SetThreadPriority(hThread, priority) == 0) {
            DWORD errCode = GetLastError();
            std::cerr << "Error setting thread priority: " << errCode << "\n";
        }
        CloseHandle(hThread);
    }
}